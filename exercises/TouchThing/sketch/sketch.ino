/* sketch.ino

- derived from Arduino ESP32 core TouchButtonV2.ino example
- this is an example how to use Touch Intrrerupts
- the sketch will tell when it is touched and then relesased as like a
  push-button
- this method based on touchInterruptGetLastStatus() is only available for
  ESP32 S2 and S3
*/

#include "Arduino.h"

int threshold = 1500;
bool touch1detected = false;
bool touch2detected = false;

void gotTouch1() { touch1detected = true; }
void gotTouch2() { touch2detected = true; }

void setup() {
  Serial.begin(115200);
  delay(1000); // time to bring up serial monitor

  Serial.println("\n ESP32 Touch Interrupt Test\n");
  touchAttachInterrupt(T14, gotTouch1, threshold); 
  touchAttachInterrupt(T8, gotTouch2, threshold);
}

void loop() {
  if (touch1detected) {
    touch1detected = false;
    if (touchInterruptGetLastStatus(T14))
        Serial.println(" --- T14 Touched");
    else
        Serial.println(" --- T14 Released");
  }
  if (touch2detected) {
    touch2detected = false;
    if (touchInterruptGetLastStatus(T8))
        Serial.println(" --- T8 Touched");
    else
        Serial.println(" --- T8 Released");
  }

  delay(80);
} 
